# eskk.vim for Pearl

eskk is pure Vim script implementation of input method engine SKK

[SKK dictionary files gh-pages](https://skk-dev.github.io/dict/)

## Details

- Plugin: https://github.com/tyru/eskk.vim
- Pearl: https://github.com/pearl-core/pearl
